from django.contrib import admin
from .models import Memento, MementoCat


admin.site.register(Memento)
admin.site.register(MementoCat)