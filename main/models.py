from django.db import models
from accounts.models import Profile



class MementoCat(models.Model):
    user = models.ForeignKey(Profile, on_delete=models.CASCADE)
    category = models.CharField(max_length=50,default='', verbose_name="Category Name")
    summary = models.CharField(max_length=250,default='', verbose_name="Category Summary")
    image = models.ImageField(upload_to="MEMENTO/IMAGES/%m/%d/", blank=True, verbose_name="Category Image")

    def __str__(self):
        return '{} <-----> {}'.format(self.category, self.user)



class Memento(models.Model):
    category = models.ForeignKey(MementoCat, on_delete=models.CASCADE, default=1)
    title = models.CharField(max_length=50, default='')
    date = models.DateField(auto_now=True)
    time = models.TimeField(auto_now=True)
    text = models.TextField()
    attach = models.FileField(upload_to="MEMENTO/FILES/%m/%d/", blank=True)
    image = models.ImageField(upload_to="MEMENTO/IMAGES/%m/%d/", blank=True)

    def __str__(self):
        return '{} <-----> {}'.format(self.title, self.category.user)