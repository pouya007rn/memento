from django.shortcuts import render, redirect, get_object_or_404, HttpResponse, Http404
from .models import Memento, MementoCat
from accounts.models import Profile
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from .forms import CategoryForm, MementoSearch, MementoForm
from django.views.generic import UpdateView

def index(request):
    try:
        pro = Profile.objects.filter(user=request.user)
    except:
        pro = None
    return render(request, 'index.html', {"profile": pro})

@login_required()
def profile(request, slug):

    if str(request.user) == str(slug):
        user = get_object_or_404(User, username=slug)
        pro = Profile.objects.filter(user=user)

        return render(request, 'profile.html', {"profile": pro})
    else:
       raise Http404



@login_required()
def memento_category(request):

    cat = MementoCat.objects.filter(user__user=request.user)
    pro = Profile.objects.filter(user=request.user)

    return render(request, 'memcat.html', {'category': cat, 'profile': pro})

@login_required()
def memento(request,slug):

    cat = get_object_or_404(MementoCat, id=slug)
    if str(request.user) == str(cat.user):
        pro = Profile.objects.filter(user=request.user)
        mem = Memento.objects.filter(category__id=slug)

        return render(request, 'memlist.html', {'category': cat, 'profile': pro, 'memento': mem})

    else:

        raise Http404


@login_required()
def memento_detail(request,pk):

    memento = get_object_or_404(Memento, id=pk)
    if str(request.user) == str(memento.category.user):
        pro = Profile.objects.filter(user=request.user)
        mem = Memento.objects.filter(id=pk)

        return render(request, 'memento.html', {'profile': pro, 'memento': mem, 'meme': memento})

    else:

        raise Http404





@login_required()
def delete_category(request,slug):
    category = MementoCat.objects.get(id=slug)
    if str(request.user) == str(category.user):

        category.delete()
        return redirect("memcat")
    else:
        raise Http404



@login_required()
def delete_memento(request,slug):
    mem = Memento.objects.get(id=slug)
    if str(request.user) == str(mem.category.user):
        mem.delete()
        return redirect("/memento-category/{}/".format(mem.category.id))
    else:
        raise Http404


@login_required()
def add_category(request):

    user = Profile.objects.get(user=request.user)

    if request.method == "POST":

        form = CategoryForm(request.POST, request.FILES)

        if form.is_valid():

            category = request.POST.get('category')
            summary = request.POST.get('summary')
            if 'image' in request.FILES:
                image = request.FILES['image']
            else:
                image = None

            cat = MementoCat.objects.create(user=user, category=category,
                                              summary=summary, image=image)

            cat.save()

            return redirect('memcat')

    else:
        form = CategoryForm()

    return render(request, 'catadd.html', {'form': form})




def add_memento(request,slug):

    cat = get_object_or_404(MementoCat, id=slug)

    if str(request.user) == str(cat.user):

        if request.method == "POST":

            form = MementoForm(request.POST, request.FILES)

            if form.is_valid():

                title = request.POST.get('title')
                text  = request.POST.get('text')
                if 'image' in request.FILES:
                    image = request.FILES['image']
                else:
                    image = None

                if 'attach' in request.FILES:
                    attach = request.FILES['attach']
                else:
                    attach = None

                mem = Memento.objects.create(category=cat, title=title,
                                                image=image,
                                                text=text,
                                                attach=attach)

                mem.save()

                return redirect('/memento-category/{}/'.format(cat.id))

        else:
            form = MementoForm()

        return render(request, 'memadd.html', {'form': form})



@login_required()
def memento_list(request):
    pro = Profile.objects.filter(user=request.user)
    list = Memento.objects.filter(category__user__user=request.user).order_by('-time')
    if request.method == "POST":

        search_form = MementoSearch(request.POST or None)

        if search_form.is_valid():
            inp = request.POST.get('input')

            if inp is None:
                list = Memento.objects.filter(category__user__user=request.user).order_by('-time')
            else:

                title = Memento.objects.filter(category__user__user=request.user).filter(title__icontains=inp).order_by('-time')
                category = Memento.objects.filter(category__user__user=request.user).filter(category__category__icontains=inp).order_by('-time')

                list = title | category

            return render(request, 'allmem.html', {'memento': list, 'profile': pro, 'form': search_form})
    else:

        search_form = MementoSearch()

        return render(request, 'allmem.html',{'memento': list, 'profile': pro, 'form': search_form})





class EditCategory(UpdateView):

    template_name = 'catedit.html'
    model = MementoCat
    form_class = CategoryForm

    def get_success_url(self):
        return '/memento-category/'

    def get(self, request, pk):

        cat = get_object_or_404(MementoCat, pk=pk)

        if int(cat.user.id) == int(request.user.id):

            pro = Profile.objects.filter(user=request.user)

            form = CategoryForm(initial={"summary": cat.summary, "category": cat.category, "image": cat.image})

            return render(request, 'catedit.html', {'profile': pro, 'form': form})

        else:
            raise Http404

    def put(self, request, pk, *args, **kwargs):

        cat = get_object_or_404(MementoCat, pk=pk)
        if int(cat.user.id) == int(request.user.id):

            form = CategoryForm(request.POST, request.FILES)

            if form.is_valid():
                form.save()
                image = request.FILES['image']
                summary = request.POST.get('summary')
                category = request.POST.get('category')

                cat.image = image
                cat.summary = summary
                cat.category = category

                cat.save()

                return '/memento-category/{}/'.format(cat.id)

        else:
            raise Http404




class EditMemento(UpdateView):

    template_name = 'memedit.html'
    model = Memento
    form_class = MementoForm

    def get_success_url(self):

        return '/memento-category/memento/{}/'.format(self.request.headers.get('Referer')[-3:-1])

    def get(self, request, pk):

        mem = get_object_or_404(Memento, pk=pk)

        if int(mem.category.user.id) == int(request.user.id):

            pro = Profile.objects.filter(user=request.user)

            form = MementoForm(initial={"title": mem.title, "text": mem.text, "image": mem.image, 'attach': mem.attach})

            return render(request, 'memedit.html', {'profile': pro, 'form': form})

        else:
            raise Http404

    def put(self, request, pk, *args, **kwargs):

        mem = get_object_or_404(Memento, pk=pk)
        if int(mem.category.user.id) == int(request.user.id):

            form = MementoForm(request.POST, request.FILES)

            if form.is_valid():
                form.save()
                image = request.FILES['image']
                attach = request.FILES['attach']
                text = request.POST.get('text')
                title = request.POST.get('title')

                mem.image = image
                mem.text = text
                mem.title = title
                mem.attach = attach

                mem.save()

                return '/memento-category/{}/'.format(mem.id)

        else:
            raise Http404


