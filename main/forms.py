from django import forms
from .models import Memento, MementoCat

class MementoForm(forms.ModelForm):

    class Meta:

        model = Memento

        fields = ('title', 'text', 'attach', 'image')


class CategoryForm(forms.ModelForm):
    class Meta:
        model = MementoCat

        fields = ('category', 'summary', 'image')


class MementoSearch(forms.Form):

    input = forms.CharField(max_length=100, label="Search", required=False)
