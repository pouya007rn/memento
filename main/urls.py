from django.urls import path
from . import views

urlpatterns = [
        path('', views.index, name='home'),
        path('profile/<slug>/', views.profile, name='profile'),
        path('memento-category/', views.memento_category, name='memcat'),
        path('memento-category/<slug>/', views.memento, name='memlist'),
        path('memento-category/memento/<pk>/', views.memento_detail, name='memento'),
        path('category-delete/<slug>/', views.delete_category, name="cat-delete"),
        path('memento-delete/<slug>/', views.delete_memento, name="mem-delete"),
        path('add-category/', views.add_category, name="catadd"),
        path('add-memento/<slug>/', views.add_memento, name='memadd'),
        path('edit-category/<pk>/', views.EditCategory.as_view(), name='catedit'),
        path('memento-all/', views.memento_list, name='memall'),
        path('edit-memento/<pk>/', views.EditMemento.as_view(), name='memedit'),
]