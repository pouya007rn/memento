from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver



class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    birthdate = models.DateField(null=True)
    gender = models.CharField(max_length=25,choices=(('male','male'), ('female','female'), ('other','other')), null=True)
    email = models.EmailField(max_length=150, null=True)
    image = models.ImageField(upload_to="images/profile/", blank=True, null=True, default='/default/avatar.png')
    bio = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.user.username




@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()