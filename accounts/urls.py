from django.urls import path
from . import views

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logout_request, name='logout'),
    path('password-reset/', views.change_password, name='change_password'),
    path('edit/<pk>/', views.edit_profile.as_view(), name='edit'),
]
