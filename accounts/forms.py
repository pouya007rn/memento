from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Profile

class SignUpForm(UserCreationForm):
    Email = forms.EmailField()
    birthdate = forms.DateField()
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)

    class Meta:
        model = User
        fields = ('username', 'Email', 'birthdate', 'first_name', 'last_name', 'password1', 'password2', )


class Loginform(forms.Form):
    username = forms.CharField(max_length= 25,label="Enter username")
    password = forms.CharField(max_length= 30, label='Password', widget=forms.PasswordInput)


class ProfileForm(forms.ModelForm):
   #username = forms.CharField(max_length=50, required=False, label='Username')
   #Image = forms.ImageField(required=False)

   class Meta:

       model = Profile

       fields = ( 'first_name', 'last_name', 'birthdate', 'gender', 'email', 'image', 'bio')










