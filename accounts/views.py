from django.shortcuts import render, redirect, Http404
from django.contrib.auth import login,logout, authenticate
from memento.settings import EMAIL_HOST_USER
from . import forms
from django.views.generic import UpdateView
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from .models import Profile
from django.core.mail import send_mail



def loginPage(request):

    form = forms.Loginform(request.POST or None)
    if form.is_valid():
        uservalue = form.cleaned_data.get("username")
        passwordvalue = form.cleaned_data.get("password")

        user = authenticate(username=uservalue, password=passwordvalue)
        if user is not None:
            login(request, user)


            context = {'form': form,
                       'error': 'The login has been successful'}

            return redirect('/profile/{}/'.format(request.user))
        else:
            context = {'form': form,
                       'error': 'The username and password combination is incorrect'}

            return render(request, 'login.html', context)

    else:
        context = {'form': form}
        return render(request, 'login.html', context)


@login_required
def logout_request(request):
    logout(request)
    return redirect("/")


def signup(request):
    form = forms.SignUpForm(request.POST)
    if form.is_valid():
        user = form.save()
        user.refresh_from_db()
        user.profile.birthdate = form.cleaned_data.get('birthdate')
        user.profile.first_name = form.cleaned_data.get('first_name')
        user.profile.last_name = form.cleaned_data.get('last_name')
        #user.profile.gender = form.cleaned_data.get('gender')
        user.profile.email = form.cleaned_data.get('Email')
        user.save()
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password1')
        user = authenticate(username=username, password=password)
        login(request, user)
        send_mail(
            'Welcome To Memento',
            'Dear {}, Here we share our memories and record our private mementos in a new way! Hope you enjoy Memento ;)'.format(username),
            EMAIL_HOST_USER,
            ['{}'.format(user.profile.email)],
            fail_silently=False,
        )
        return redirect('/profile/{}/'.format(request.user))
    else:
        form = forms.SignUpForm()
    return render(request, 'signup.html', {'form': form})

@login_required()
def change_password(request):
    profile = Profile.objects.get(id=request.user.id)
    pro = Profile.objects.filter(user=request.user)
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            messages.success(request, 'Your password was successfully updated!')
            print(profile.email)
            send_mail(
                'Password Was Successfully Updated',
                'Dear {}, You have Successfully Updated your Password\n\n<Memento Team>'.format(
                    request.user),
                EMAIL_HOST_USER,
                ['{}'.format(profile.email)],
                fail_silently=False,
            )
            return redirect('/profile/{}/'.format(request.user))
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'resetpass.html', {
        'form': form, 'profile': pro
    })



class edit_profile(UpdateView):


        model = Profile
        form_class = forms.ProfileForm
        template_name = 'profile_edit.html'



        def get_success_url(self):
                return '/profile/{}/'.format(self.request.user)




        def get(self,request,pk):

            if int(request.user.id) == int(pk):
                init = Profile.objects.get(pk=pk)
                pro = Profile.objects.filter(user=request.user)
                form = forms.ProfileForm( initial={"username":request.user, "first_name": init.first_name,
                                                   "last_name": init.last_name, "birthdate": init.birthdate,
                                                   "gender": init.gender, "email": init.email, "image": init.image,
                                                   "bio": init.bio})


                return render(request,'profile_edit.html',{'profile': pro, 'form': form})
            else:
                raise Http404




        def put(self, request, pk, *args, **kwargs):


            if int(request.user.id) == int(pk):

                form = forms.ProfileForm(request.POST, request.FILES)

                if form.is_valid():

                    first_name = request.POST.get('first_name')
                    last_name = request.POST.get('last_name')
                    birthdate = request.POST.get('birthdate')
                    email = request.POST.get('email')
                    bio = request.POST.get('bio')
                    gender = request.POST.get('gender')

                    profile = Profile.objects.get(pk=pk)


                    profile.image =request.FILES['image']

                    profile.refresh_from_db()

                    profile.first_name = first_name
                    profile.last_name = last_name


                    profile.birthdate = birthdate
                    profile.bio = bio
                    profile.email = email
                    profile.gender = gender

                    profile.save()


                    return redirect('/profile/{}/'.format(request.user))


            else:
                raise Http404



