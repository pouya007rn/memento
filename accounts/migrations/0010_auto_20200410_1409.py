# Generated by Django 3.0.5 on 2020-04-10 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0009_auto_20200408_1342'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='gender',
            field=models.CharField(choices=[(1, 'male'), (2, 'female'), (3, 'other')], max_length=25, null=True),
        ),
    ]
