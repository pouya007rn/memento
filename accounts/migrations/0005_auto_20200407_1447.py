# Generated by Django 3.0.5 on 2020-04-07 14:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_profile_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=models.ImageField(blank=True, default='/static/images/undraw/avatar.png', null=True, upload_to='IMAGES/PROFILE/<django.db.models.fields.related.OneToOneField>/'),
        ),
    ]
